# translation of kio_nfs.po to Telugu
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Krishna Babu K <kkrothap@redhat.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kio_nfs\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-11 00:46+0000\n"
"PO-Revision-Date: 2009-01-16 22:16+0530\n"
"Last-Translator: Krishna Babu K <kkrothap@redhat.com>\n"
"Language-Team: Telugu <en@li.org>\n"
"Language: te\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"

#: kio_nfs.cpp:152
#, kde-format
msgid "Cannot find an NFS version that host '%1' supports"
msgstr ""

#: kio_nfs.cpp:322
#, kde-format
msgid "The NFS protocol requires a server host name."
msgstr ""

#: kio_nfs.cpp:352
#, kde-format
msgid "Failed to initialise protocol"
msgstr ""

#: kio_nfs.cpp:820
#, kde-format
msgid "RPC error %1, %2"
msgstr ""

#: kio_nfs.cpp:869
#, kde-format
msgid "Filename too long"
msgstr ""

#: kio_nfs.cpp:876
#, kde-format
msgid "Disk quota exceeded"
msgstr ""

#: kio_nfs.cpp:882
#, kde-format
msgid "NFS error %1, %2"
msgstr ""

#: nfsv2.cpp:391 nfsv2.cpp:442 nfsv3.cpp:429 nfsv3.cpp:573
#, kde-format
msgid "Unknown target"
msgstr ""
